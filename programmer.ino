#define LED_PIN 13

//Special analog pins
#define Z80_BUSACK A6
#define BUTTON_PIN A7

#define SR_CLK 3
#define SR_LOAD_OUTPUT 4
#define SR_INPUT 5
#define SR_DISABLE_OUTPUT 6

#define Z80_BUSREQ 7
#define Z80_RESET 8
#define Z80_MEMREQ 10
#define Z80_WR 11

#define O6 2
#define O7 9

#define MAGIC_NUMBER 123

void setup() {
  setupPins();

  Serial.begin(9600);
}

void setupPins() {
  pinMode(LED_PIN, OUTPUT);

  pinMode(BUTTON_PIN, INPUT);
  pinMode(SR_INPUT, OUTPUT);
  pinMode(SR_LOAD_OUTPUT, OUTPUT);
  digitalWrite(SR_DISABLE_OUTPUT, HIGH);
  pinMode(SR_DISABLE_OUTPUT, OUTPUT);
  pinMode(SR_CLK, OUTPUT);

  //Ensure that its high from the start so we dont confuse the cpu
  digitalWrite(Z80_BUSREQ, HIGH);
  pinMode(Z80_BUSREQ, OUTPUT);
  digitalWrite(Z80_RESET, HIGH);
  pinMode(Z80_RESET, OUTPUT);
  tristate(Z80_BUSACK);
  tristate(Z80_MEMREQ);
  tristate(Z80_WR);

  tristateDataOutputPins();
}

void enableDataOutputPins() {
  for (int i = A0; i <= A5; i++) {
    pinMode(i, OUTPUT);
  }

  pinMode(O6, OUTPUT);
  pinMode(O7, OUTPUT);
}

void tristateDataOutputPins() {
  for (int i = A0; i <= A5; i++) {
    tristate(i);
  }

  tristate(O6);
  tristate(O7);
}

void specialAwaitPinState(int pin, int state) {
  while (specialDigitalRead(pin) != state) {}
}

void loadShiftOutput() {
  digitalWrite(SR_LOAD_OUTPUT, HIGH);
  delay(1);
  digitalWrite(SR_LOAD_OUTPUT, LOW);  
}

void shiftBit(int value) {
  digitalWrite(SR_INPUT, value);
  digitalWrite(SR_CLK, HIGH);
  delay(1);
  digitalWrite(SR_CLK, LOW);
}

void shiftNumber(int num) {
  for (int i = 15; i >= 0; i--) {
    shiftBit(bitRead(num, i));
  }
}

void tristate(int pin) {
  pinMode(pin, INPUT);
  digitalWrite(pin, LOW);
}

void setDataOut(int num) {
  for (int i = 0; i < 6; i++) {
    digitalWrite(A0 + i, bitRead(num, i));
  }

  digitalWrite(O6, bitRead(num, 6));
  digitalWrite(O7, bitRead(num, 7));
}

void writeByte(int address, int data) {
  setDataOut(data);
  shiftNumber(address);
  loadShiftOutput();

  digitalWrite(Z80_WR, LOW);
  delay(1);
  digitalWrite(Z80_WR, HIGH);
}

void writeProgram(int programSize) {
  //X write low to busreq
  //X await busack to go low
  //X set data pins to outputs
  //X enable shift register output

  //for every program byte 
  //  write byte to data output pins
  //  shift into register
  //  set memreq and wr low (active)
  //  maybe wait a little
  //  set memreq and wr high (inactive)
  //  maybe wait a little

  //X disable shift register output
  //X tristate the data pins
  //X tristate mreq, and wr
  //X pull busreq high (inactive)
  //X reset the cpu
  
  digitalWrite(Z80_BUSREQ, LOW);
  specialAwaitPinState(Z80_BUSACK, LOW);
  blinkLed();

  digitalWrite(Z80_WR, HIGH);
  pinMode(Z80_WR, OUTPUT);
  
  enableDataOutputPins();
  digitalWrite(SR_DISABLE_OUTPUT, LOW);

  programFromSerial(programSize);

  digitalWrite(SR_DISABLE_OUTPUT, HIGH);
  tristateDataOutputPins();

  tristate(Z80_MEMREQ);
  tristate(Z80_WR);
  
  digitalWrite(Z80_BUSREQ, HIGH);
  specialAwaitPinState(Z80_BUSACK, HIGH);
  blinkLed();

  resetCpu();
}

void programFromSerial(int programSize) {
  while (programSize > 0) {
    programSize -= programChunkFromSerial();  
  }
}

int programChunkFromSerial() {
  int length = readSerialShort();
  int address = readSerialShort();
  byte data;

  for (int i = 0; i < length; i++) {
    data = blockingRead();

    writeByte(address++, data);
  }

  //To ensure that everything went allright
  Serial.write(data);

  return length;
}

void resetCpu() {
  digitalWrite(Z80_RESET, LOW);
  delay(2000);
  digitalWrite(Z80_RESET, HIGH);
}

void blinkLed() {
  digitalWrite(LED_PIN, HIGH);
  delay(700);
  digitalWrite(LED_PIN, LOW);
  delay(300);
}

int specialDigitalRead(int pin) {
  return analogRead(pin) >= 512 ? HIGH : LOW;
}

void loop() {
  if (Serial.available() > 0 && specialDigitalRead(BUTTON_PIN) == LOW) {
    byte stuff = Serial.read();
    if (stuff == MAGIC_NUMBER) {
      Serial.write(MAGIC_NUMBER);
      int programSize = readSerialShort();
      
      writeProgram(programSize);
    }
  } else if (Serial.available() == 0 && specialDigitalRead(BUTTON_PIN) == LOW) {
    blinkLed();
  }
}

int readSerialShort() {
  int num = 0;
  num |= ((int) blockingRead() << 8);
  num |= blockingRead();

  return num;
}

byte blockingRead() {
  while (Serial.available() <= 0) {}

  return Serial.read();
}
